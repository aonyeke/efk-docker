# EFK Docker
EFK stack for a containerized centralized logging application

## Getting Started
These instructions will help you setup the project on your system

### Prerequisites
- Docker
- Docker Compose

### Running the Container
~ 1. clone the repo
```sh
$ git clone https://bitbucket.org/aonyeke/efk-docker.git
```
~ 2. cd into directory
```sh
$ cd efk-docker
```
~ 3. create ElasticSearch data directory and change owner
```sh
$ sudo mkdir -p /var/lib/elasticsearch/
$ sudo chown 1000:1000 /var/lib/elasticsearch/
```
~ 4. run docker compose
```sh
$ docker-compose up
```
~ 5. Please launch Kibana on your browser and follow the [Kibana documentation](https://www.elastic.co/guide/en/kibana/6.3/tutorial-define-index.html#tutorial-define-index) to define your index pattern with ```fluentd-*```